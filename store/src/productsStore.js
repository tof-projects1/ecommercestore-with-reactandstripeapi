const productsArray = [
    {
        id: process.env.REACT_APP_COFFEE_KEY,
        title: "Coffee",
        price: 4.99
    },
    {
        id: process.env.REACT_APP_SUNGLASSES_KEY,
        title: "Sunglasses",
        price: 9.99
    },
    {
        id: process.env.REACT_APP_CAMERA_KEY,
        title: "Camera",
        price: 39.99
    }
];

function getProductData(id) {
    let productData = productsArray.find(product => product.id === id);

    if (productData == undefined) {
        console.log("Product data does not exist for ID: " + id);
        return undefined;
    }

    return productData;
}

export { productsArray, getProductData };