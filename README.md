**React and Stripe Shopping Cart Application**

This repository contains a full-stack shopping cart application built using ReactJS, Express.js and Stripe for payment processing. The application is designed to provide a seamless shopping experience with features such as responsive product display, dynamic cart management, and secure payment processing through Stripe.

**Application Preview**:

![app-preview](https://gitlab.com/tof-projects1/ecommercestore-with-reactandstripeapi/-/raw/main/images/app-preview.png?ref_type=heads)

**Features**
1. Navbar and Container Setup: The application starts with a well-structured navbar and container layout, providing a clean and organized user interface.
2. Responsive Rows and Columns: Utilizing React's capabilities, the product display is implemented with responsive rows and columns, ensuring a user-friendly experience across different devices.
3. Dynamic Product Rendering: The product display is generated dynamically using the map function in React. This allows for easy scalability and maintenance as the product array evolves.
4. State Management and Components: Various React components are created to manage state within the application, enabling efficient updates and reactivity. The cart provider function is implemented to oversee the total cost and provide context access to the shopping cart.
5. Cart Functionality: The application supports adding products to the cart, managing product quantities, calculating total product count using array reduction, and displaying the total price. Additionally, users can remove items from the cart with ease.
6. Express Server Integration: The project includes an Express server to handle post requests containing React data. This server-side interaction enhances security and facilitates seamless communication between the front end and back end.
7. Stripe Integration: A Stripe account is created, and test products are added. The integration involves creating an Express server and making checkout requests.


**Getting Started**:
- Clone this repository
- Install dependencies: npm install
- Start the React development server by first running:"cd store" in the root directory to move into the store folder. 
- Then in the store directory, run: npm start
- Then open a new terminal, and navigate into the root directory of the project folder to start the Express.js server by running the command: node server.js

Feel free to explore the codebase, customize the application to meet your specific requirements, and enhance the shopping experience for your users. Happy coding!

